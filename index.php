<?php 
    require "classes/db.php";
    require "classes/Product.php";
    require "classes/Dvd.php";
    require "classes/Book.php";
    require "classes/Furniture.php";
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
    <title>Document</title>
</head>
    <header>
        <div class="clearfix container mt-3">
            <h2 class="float-left">Product List</h2>
            <button type="submit" name="DeleteAction" form="deleteForm" class="btn btn-secondary float-right ml-2">Delete Selected Products</button>
            <a href="AddProductForm.html" class="mt-2 mt-md-0 btn btn-primary float-right">Add New Product</a>
        </div> 
        <div class="container">
            <hr>
        </div>
    </header>
<body>
    <div class="container">
        <?php
            if(isset($_GET['msg'])){
                if($_GET['msg'] == 'added'){
                    $msg = "New Product has bees succesfully added";
                    include "includes/successAlert.php";
                }
                if($_GET['msg'] == 'noDel'){
                    $msg = "You haven't selected any products to delete";
                    include "includes/errorAlert.php";
                }
            }
        ?>


        <form action="php/DeleteProduct.php" id="deleteForm" method="post">
        <div class="row ">
            <?php 
                $select = baseProduct::getProduct();
                while ($row = $select->fetch()){
            ?>
            <div class="mb-3 col-sm-6 col-md-4 col-lg-3">
                <div class="card flex-item">
                    <div class="card-header">
                        <input type="checkbox" name="ProductsToDelete[]" value="<?= $row['id'] ?>" id="">
                    </div>
                    <div class="card-body text-center">
                        <h4 class="card-title"> <?= $row['SKU'] ?> </h4>
                        <p class="cad-text"><?= $row['name'] ?></p>
                        <p class="cad-text"><?= $row['price'] ?> $</p>
                        <p class="cad-text"> <?= $row['spAttribute'] ?> </p>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
        </form>
    </div>
    
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>