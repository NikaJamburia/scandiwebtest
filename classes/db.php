<?php
    class Db{
        private $host = "localhost";
        private $user = "root";
        private $pass = "";
        private $db = "ScandiwebProducts";
        public static $conn = null;

        public function __construct(){
            if(self::$conn === NULL){
                $dsn = 'mysql:host='.$this->host.';dbname='.$this->db;
                self::$conn = new PDO($dsn, $this->user, $this->pass);
                self::$conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            }  
        }
    

    }

?>