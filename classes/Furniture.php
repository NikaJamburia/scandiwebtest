<?php
    class Furniture extends baseProduct{
        public function __construct($sku, $name, $price, $height, $width, $length){
                $this->attr = "Dimentions: {$height}x{$width}x{$length}";
                parent::__construct($sku, $name, $price);
        }

        public function setProduct(){
            $db = new Db;
            $insert = "INSERT INTO products (SKU, name, price, type, spAttribute) VALUES (:sku, :name, :price, :type, :spAttribute)";
            $statement = $db::$conn->prepare($insert);
            $statement->execute(['sku' => $this->SKU, 'name' => $this->Name, 'price' => $this->Price, 'type' => "Furniture", 'spAttribute' => $this->attr]);
        }

    }
?>