<?php
    class Book extends baseProduct{
        public function __construct($sku, $name, $price, $attr){
                $this->attr = "Weight: $attr KGs";
                parent::__construct($sku, $name, $price);
        }

        public function setProduct(){
            $db = new Db;
            $insert = "INSERT INTO products (SKU, name, price, type, spAttribute) VALUES (:sku, :name, :price, :type, :spAttribute)";
            $statement = $db::$conn->prepare($insert);
            $statement->execute(['sku' => $this->SKU, 'name' => $this->Name, 'price' => $this->Price, 'type' => "Book", 'spAttribute' => $this->attr]);
        }

    }
?>