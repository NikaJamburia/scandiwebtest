<?php
    class Dvd extends baseProduct{
        public function __construct($sku, $name, $price, $attr){
                $this->attr = "Size: $attr MBs";
                parent::__construct($sku, $name, $price);
        }

        public function setProduct(){
            $db = new Db;
            $insert = "INSERT INTO products (SKU, name, price, type, spAttribute) VALUES (:sku, :name, :price, :type, :spAttribute)";
            $statement = $db::$conn->prepare($insert);
            $statement->execute(['sku' => $this->SKU, 'name' => $this->Name, 'price' => $this->Price, 'type' => "DVD-disk", 'spAttribute' => $this->attr]);
        }

    }
?>