window.addEventListener("load", function(){
    $("#saveBtn").click(function(event){
        error = false;
        var errorMsg = {sku: "", name: "", price: "", size: "", weight: "", height: "", width: "", leng: ""};

        // SKU Validation
        if($("#sku").val() == ""){
            errorMsg.sku = "Please fill out this field";
            error = true;
            
            $("#sku").addClass("redBorder");
            $("#skuError").html(errorMsg.sku); 
        }
        else{
            $("#sku").removeClass("redBorder");
            $("#skuError").html(""); 
        }
        
        // Name Validation
        if($("#name").val() == ""){
            errorMsg.name = "Please fill out this field";
            error = true;

            $("#name").addClass("redBorder");
            $("#nameError").html(errorMsg.name); 
        }
        else if(($("#name").val().length) >= 50){
            errorMsg.name = "This field must contain less then 50 symbols";
            error = true; 

            $("#name").addClass("redBorder");
            $("#nameError").html(errorMsg.name);
        }
        else{
            $("#name").removeClass("redBorder");
            $("#nameError").html(""); 
        }

        //Price Validation
        if($("#price").val() == ""){
            errorMsg.price = "Please fill out this field";
            error = true;

            $("#price").addClass("redBorder");
            $("#priceError").html(errorMsg.price); 
        }
        else if($("#price").val().length > 0){
            chars = "abcdefghijklmnopqrstuvwxyz$%^&*,'+==/@#";
            str = $("#price").val();
            for(i=0;i<chars.length;i++){
                if(str.indexOf(chars[i]) != -1){
                    errorMsg.price = "This field can only contain numbers!";
                    error = true;

                    $("#price").addClass("redBorder");
                    $("#priceError").html(errorMsg.price);
                    break;
                }
                else{
                    $("#price").removeClass("redBorder");
                    $("#priceError").html(""); 
                }
            }
        }

        //Disk size Validation
        if(typeof($("#size").val()) !== 'undefined'){
            if($("#size").val() == ""){
                console.log(1);
                errorMsg.size = "Please fill out this field";
                error = true;
    
                $("#size").addClass("redBorder");
                $("#sizeError").html(errorMsg.size); 
            }
            else if($("#size").val().length > 0){
                chars = "abcdefghijklmnopqrstuvwxyz$%^&*,'+==/@#";
                str = $("#size").val();
                for(i=0;i<chars.length;i++){
                    if(str.indexOf(chars[i]) != -1){
                        errorMsg.size = "This field can only contain numbers!";
                        error = true;
    
                        $("#size").addClass("redBorder");
                        $("#sizeError").html(errorMsg.size);
                        break;
                    }
                    else{
                        $("#size").removeClass("redBorder");
                        $("#sizeError").html(""); 
                    }
                }
            }
        }
        

        //weight Validation
        if(typeof($("#weight").val()) !== 'undefined'){
            if($("#weight").val() == ""){
                console.log(1);
                errorMsg.weight = "Please fill out this field";
                error = true;
    
                $("#weight").addClass("redBorder");
                $("#weightError").html(errorMsg.weight); 
            }
            else if($("#weight").val().length > 0){
                chars = "abcdefghijklmnopqrstuvwxyz$%^&*,'+==/@#";
                str = $("#weight").val();
                for(i=0;i<chars.length;i++){
                    if(str.indexOf(chars[i]) != -1){
                        errorMsg.weight = "This field can only contain numbers!";
                        error = true;
    
                        $("#weight").addClass("redBorder");
                        $("#weightError").html(errorMsg.weight);
                        break;
                    }
                    else{
                        $("#weight").removeClass("redBorder");
                        $("#weightError").html(""); 
                    }
                }
            }
        }
        
        //HxWxL valitadion
        if(typeof($("#height").val()) !== 'undefined'){
            if($("#height").val() == ""){
                console.log(1);
                errorMsg.height = "Please fill out this field";
                error = true;
    
                $("#height").addClass("redBorder");
                $("#heightError").html(errorMsg.height); 
            }
            else if($("#height").val().length > 0){
                chars = "abcdefghijklmnopqrstuvwxyz$%^&*,'+==/@#";
                str = $("#height").val();
                for(i=0;i<chars.length;i++){
                    if(str.indexOf(chars[i]) != -1){
                        errorMsg.height = "This field can only contain numbers!";
                        error = true;
    
                        $("#height").addClass("redBorder");
                        $("#heightError").html(errorMsg.height);
                        break;
                    }
                    else{
                        $("#height").removeClass("redBorder");
                        $("#heightError").html(""); 
                    }
                }
            }
        }

        if(typeof($("#width").val()) !== 'undefined'){
            if($("#width").val() == ""){
                console.log(1);
                errorMsg.width = "Please fill out this field";
                error = true;
    
                $("#width").addClass("redBorder");
                $("#widthError").html(errorMsg.width); 
            }
            else if($("#width").val().length > 0){
                chars = "abcdefghijklmnopqrstuvwxyz$%^&*,'+==/@#";
                str = $("#width").val();
                for(i=0;i<chars.length;i++){
                    if(str.indexOf(chars[i]) != -1){
                        errorMsg.width = "This field can only contain numbers!";
                        error = true;
    
                        $("#width").addClass("redBorder");
                        $("#widthError").html(errorMsg.width);
                        break;
                    }
                    else{
                        $("#width").removeClass("redBorder");
                        $("#widthError").html(""); 
                    }
                }
            }
        }

        if(typeof($("#leng").val()) !== 'undefined'){
            if($("#leng").val() == ""){
                console.log(1);
                errorMsg.leng = "Please fill out this field";
                error = true;
    
                $("#leng").addClass("redBorder");
                $("#lengError").html(errorMsg.length); 
            }
            else if($("#leng").val().length > 0){
                chars = "abcdefghijklmnopqrstuvwxyz$%^&*,'+==/@#";
                str = $("#leng").val();
                for(i=0;i<chars.length;i++){
                    if(str.indexOf(chars[i]) != -1){
                        errorMsg.leng = "This field can only contain numbers!";
                        error = true;
    
                        $("#leng").addClass("redBorder");
                        $("#lengError").html(errorMsg.leng);
                        break;
                    }
                    else{
                        $("#leng").removeClass("redBorder");
                        $("#lengError").html(""); 
                    }
                }
            }
        }

        //Preventing Submit
        if(error){
            event.preventDefault();
        }

    })
}) 
