<?php 
    require "../classes/db.php";
    require "../classes/Product.php";
    //$db = new Db;

    if(isset($_POST['DeleteAction'])){
        if(isset($_POST['ProductsToDelete'])){
            baseProduct::deleteProduct($_POST['ProductsToDelete']);
            header("Location:../index.php");
        }
        else{
            $msg = "You didn't select any products to delete";
            include "includes/errorAlert.php";
            header("Location:../index.php?msg=noDel");
        }
    }
?>