<?php
    require "../classes/db.php";
    require "../classes/Product.php";
    require "../classes/Dvd.php";
    require "../classes/Book.php";
    require "../classes/Furniture.php";

    $POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

    $newSKU = $POST['newSKU'];
    $newName = $POST['newName'];
    $newPrice = $POST['newPrice'];
    $newType = $POST['newType'];

    switch ($newType){
        case "DVD-disk":
            $newAttr = $POST['newAttr'];

            $newProduct = new Dvd($newSKU, $newName, $newPrice, $newAttr);
            $newProduct->setProduct();
            break;
        case "Book":
            $newAttr = $POST['newAttr'];

            $newProduct = new Book($newSKU, $newName, $newPrice, $newAttr);
            $newProduct->setProduct();
            break;
        case "Furniture":
            $newHeight = $POST['newHeight'];
            $newWidth = $POST['newWidth'];
            $newLength = $POST['newLength'];

            $newProduct = new Furniture($newSKU, $newName, $newPrice, $newHeight, $newWidth, $newLength);
            $newProduct->setProduct();
            break;
    }


    header("Location:../index.php?msg=added");
?>