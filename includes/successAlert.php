<div class="alert alert-success alert-dismissible my-2 fade show">
    <button type="button" class="close" data-dismiss="alert">&times;</button>
    <span><?= $msg ?></span>
</div>
        